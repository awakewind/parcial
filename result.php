<!DOCTYPE html>

<?php 

$mes = "12";
$pagar = $_GET['pagar'];
$descuento;
$iva = 0.13;
$total;

if ($mes >= "01" and $mes <= "06"){
	if ($pagar > 1500){
		$descuento = 0.10;
		$total = $pagar - ($pagar * $descuento);
		
	}else{
		$total = ($pagar * $iva) + $pagar;
		
	}
}else{
	if ($pagar > 1000 and $pagar <=2000){
		$descuento = 0.10;
		$total = (($pagar - ($pagar * $descuento))*$iva) + ($pagar - ($pagar * $descuento));
		
	}else if ($pagar > 2000 && $pagar <=5000){
		$descuento = 0.20;
		$total = $pagar - ($pagar * $descuento);
		
	}else if ($pagar > 5000){
		$descuento = 0.10;
		$total = (($pagar - ($pagar * $descuento))*$iva) + ($pagar - ($pagar * $descuento)) ;
	}else{
		$total = ($pagar * $iva) + $pagar;		
	}
}
?>
<html lang="en">
<head>
	<title>Contact V2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="bg-contact2" style="background-image: url('images/bg-01.jpg');">
		<div class="container-contact2">
			<div class="wrap-contact2">
				<form class="contact2-form validate-form" method="get" action="script1.php">
					<span class="contact2-form-title">
						El total a pagar es <?php echo $total;?>
					</span>

					

					
				</form>
			</div>
		</div>
	</div>




<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
	</script>

</body>
</html>